//
//  DeviceUtil.swift
//  SapoAdminIOS
//
//  Created by ThangTM-PC on 11/27/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import UIKit

public class DeviceUtils {
    
    public static func deviceUniqueId() -> String {
        let defaults = UserDefaults.standard
        if let deviceUniqueId: String = defaults.string(forKey: "device_unique_id") {
            return deviceUniqueId
        }
        
        let deviceUniqueId = "IOS-\(UIDevice.current.identifierForVendor!.uuidString)"
        defaults.set(deviceUniqueId, forKey: "device_unique_id")
        defaults.synchronize()
        
        return deviceUniqueId
    }
    
    public static func os() -> String {
        return "iOS"
    }
    
    public static func osVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    public static func languageCode() -> String {
        return NSLocale.current.languageCode!
    }
    
    public static func appVersion() -> String {
        return (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!
    }
    
}
